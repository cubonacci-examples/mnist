import numpy as np
import mnist
import time


class DataLoader:
    def __init__(self):
        pass

    @staticmethod
    def load_training_data():
        retries = 0
        while True:
            try:
                train_images = mnist.train_images()
                train_labels = mnist.train_labels()
                break
            except Exception as e:
                if retries < 10:
                    retries += 1
                    time.sleep(15)
                else:
                    raise e
        X_train = train_images / 255.
        y_train = np.zeros((train_images.shape[0], 10))
        y_train[np.arange(train_images.shape[0]), train_labels] = 1
        return X_train, y_train
