import tensorflow as tf
from keras.models import Model as KerasModel
from keras.layers import Dense,Input
from keras.layers import Dropout
from keras.layers import Flatten
from keras.constraints import maxnorm
from keras.layers.convolutional import Convolution2D
from keras.layers.convolutional import MaxPooling2D
from keras.callbacks import EarlyStopping

import pandas as pd
import numpy as np

from typing import Optional


class Model:
    def __init__(self, number_neurons_layer_1: int, number_neurons_layer_2: int, number_neurons_dense: int, dropout_rate: float, activation: str):
        self.number_neurons_layer_1 = number_neurons_layer_1
        self.number_neurons_layer_2 = number_neurons_layer_2
        self.number_neurons_dense = number_neurons_dense
        self.dropout_rate = dropout_rate
        self.activation = activation
        self.model: Optional[KerasModel] = None
        self.build_graph()

    def build_graph(self):
        input = Input(shape=(28, 28, 1), name='input')
        with tf.name_scope('conv_1') as scope:
            conv_1 = Convolution2D(filters=self.number_neurons_layer_1,
                                   kernel_size=(3, 3),
                                   border_mode='same',
                                   activation=self.activation,
                                   W_constraint=maxnorm(3))(input)
            drop_conv_1 = Dropout(self.dropout_rate)(conv_1)
            max_pool_1 = MaxPooling2D(pool_size=(2, 2))(drop_conv_1)

        with tf.name_scope('conv_1') as scope:
            conv_2 = Convolution2D(filters=self.number_neurons_layer_2,
                                   kernel_size=(3, 3),
                                   border_mode='same',
                                   activation=self.activation,
                                   W_constraint=maxnorm(3))(max_pool_1)
            drop_conv_2 = Dropout(self.dropout_rate)(conv_2)
            max_pool_2 = MaxPooling2D(pool_size=(2, 2))(drop_conv_2)

        with tf.name_scope('dense') as scope:
            flat = Flatten()(max_pool_2)
            dense = Dense(self.number_neurons_dense,
                          activation=self.activation,
                          W_constraint=maxnorm(3))(flat)
            drop_dense = Dropout(self.dropout_rate)(dense)

            out = Dense(10, activation='softmax')(drop_dense)

        self.model = KerasModel(input, out)
        self.model.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=['accuracy'])

    def fit(self, X, y):
        early_stopping = EarlyStopping(monitor='val_loss', mode='min')
        X = np.expand_dims(X, axis=3)
        X_train, X_calibrate = X[1000:], X[:1000]
        y_train, y_calibrate = y[1000:], y[:1000]
        self.model.fit(x=X_train, y=y_train,
                       validation_data=(X_calibrate, y_calibrate),
                       batch_size=16, epochs=20, verbose=2, callbacks=[early_stopping])

    def predict(self, X):
        predictions = self.model.predict(x=np.expand_dims(X, axis=3))
        return pd.DataFrame(predictions, columns=range(10))
