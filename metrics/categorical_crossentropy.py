import numpy as np


def categorical_crossentropy(y_true, y_pred):
    y_pred_logged = np.log(y_pred.values)
    masked_y_pred_logged = y_pred_logged * y_true
    return -np.mean(np.sum(masked_y_pred_logged, axis=1))
